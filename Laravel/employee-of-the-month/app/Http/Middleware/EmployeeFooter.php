<?php
namespace App\Http\Middleware;

use Closure;
use App\Employee;

class EmployeeFooter
{
    public function handle($request, Closure $next)
    {
        $employee = Employee::findOrFail(1);
        view()->share('employee', $employee);
        return $next($request);
    }
}