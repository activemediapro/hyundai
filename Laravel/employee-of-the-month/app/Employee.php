<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['id', 'name', 'job', 'phone', 'image', 'visible'];

    protected $picPath = '/storage/employee/';
    //

    public function getImageUrl()
    {
        return $this->picPath . $this->image;
    }
}
