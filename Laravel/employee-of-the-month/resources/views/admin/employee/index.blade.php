@extends('admin.admin')

@section('pageTitle', 'Сотрудник месяца')

@section('sidebar')
@include('admin.sidebar.main')
@endsection

@section('content')
	<table class="table">
		<caption>
			<div class="table__flex table__flex--caption">
				<h2 class="h2">Сотрудник месяца</h2>
			</div>
		</caption>
	</table>

	<form method="post" action="{{ route('admin.employee.update', $item->id) }}" enctype="multipart/form-data">
		@method('PATCH')
		@csrf
		<div class="content__line">
			{{ Form::adminInput('name', $item->name, ['placeholder' => 'Введите имя', 'publicName' => 'Имя', 'required' => true]) }}
		</div>
		<div class="content__line">
			{{ Form::adminInput('job', $item->job, ['placeholder' => 'Введите должность', 'publicName' => 'Должность']) }}
		</div>
		<div class="content__line">
			{{ Form::adminInput('phone', $item->phone, ['placeholder' => 'Введите телефон', 'publicName' => 'Телефон']) }}
		</div>
		<div class="content__line">
			{{ Form::adminFile('image', $item->getImageUrl(), ['publicName' => 'Фотография (размер 225x225px)']) }}
		</div>

		<div class="content__line">
				<fieldset>
						<label class="label">Настройки показа</label>
			{{ Form::adminCheckbox('visible', $item->visible, ['id' => 'visible', 'publicName' => 'Показывается на сайте']) }}
				</fieldset>
		</div>
		<div class="content__line">
			<button class="button button--large" type="submit">Сохранить</button>
			<button class="button button--large button--grey">Отменить</button>
		</div>
	</form>
@endsection
